extern crate serde;

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct HueCliSettings {
    pub bridge_ip: Option<String>,
    pub token: Option<String>,
}

pub type Response<T> = Vec<ResponseStatus<T>>;
pub type ResourceUpdated = std::collections::HashMap<String, bool>;

#[derive(Deserialize, Debug)]
pub enum ResponseStatus<T> {
    #[serde(rename = "success")]
    Success(T),
    #[serde(rename = "error")]
    Error(Error),
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Error {
    #[serde(rename = "type")]
    pub error_code: i32,
    pub address: String,
    pub description: String,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct CreateUser {
    pub devicetype: String,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct CreatedUser {
    pub username: String,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct BridgeConfig {
    pub lights: HashMap<String, Light>,
    pub groups: HashMap<String, Group>,
    pub config: Config,
    #[serde(skip)]
    pub schedules: (),
    pub scenes: Option<HashMap<String, Scene>>,
    #[serde(skip)]
    pub rules: (),
    #[serde(skip)]
    pub sensors: (),
    #[serde(skip)]
    pub resourcelinks: (),
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Light {
    pub state: LightState,
    #[serde(rename = "type")]
    pub _type: String,
    pub name: String,
    pub modelid: String,
    pub manufacturername: Option<String>,
    pub uniqueid: String,
    pub swversion: String,
}

impl fmt::Display for Light {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} ({}) / {}:\n\t{}",
            self.name, self.modelid, self.uniqueid, self.state
        )
    }
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct LightState {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub on: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bri: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hue: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sat: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub effect: Option<LightEffect>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub xy: Option<Vec<f32>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ct: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub colormode: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reachable: Option<bool>,
}

enum_str!(LightEffect {
    None("none"),
    ColorLoop("colorloop"),
});

impl fmt::Display for LightState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} bri: {}/254 hue: {} sat: {} ct: {}",
            match self.on {
                Some(true) => "on ",
                Some(false) => "off",
                None => "<unkn>",
            },
            self.bri.map_or(String::from("<unkn>"), |c| c.to_string()),
            self.hue.map_or(String::from("<unkn>"), |c| c.to_string()),
            self.sat.map_or(String::from("<unkn>"), |c| c.to_string()),
            self.ct.map_or(String::from("<unkn>"), |c| c.to_string()),
        )
    }
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Group {
    pub name: String,
    pub lights: Vec<String>,
    #[serde(rename = "type")]
    pub _type: String,
    pub state: GroupState,
    pub class: String,
    pub action: GroupAction,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct GroupState {
    pub all_on: bool,
    pub any_on: bool,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct GroupAction {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub on: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bri: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hue: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sat: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub effect: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub xy: Option<Vec<f32>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ct: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub colormode: Option<String>,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Config {
    pub name: String,
    pub zigbeechannel: Option<i64>,
    pub bridgeid: String,
    pub mac: String,
    pub dhcp: bool,
    pub ipaddress: String,
    pub netmask: String,
    pub gateway: String,
    pub proxyaddress: String,
    pub proxyport: i64,
    #[serde(rename = "UTC")]
    pub _utc: String,
    pub localtime: Option<String>,
    pub timezone: Option<String>,
    pub modelid: Option<String>,
    pub datastoreversion: Option<String>,
    pub swversion: String,
    pub apiversion: String,
    pub swupdate: SwUpdateState,
    pub linkbutton: bool,
    pub portalservices: bool,
    pub portalconnection: Option<String>,
    pub portalstate: Option<PortalState>,
    pub factorynew: Option<bool>,
    pub replacesbridgeid: Option<String>,
    pub backup: Option<Backup>,
    pub whitelist: HashMap<String, User>,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct SwUpdateState {
    pub updatestate: i64,
    pub checkforupdate: Option<bool>,
    pub devicetypes: Option<UpdatableDevices>,
    pub url: String,
    pub text: String,
    pub notify: bool,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct UpdatableDevices {
    pub bridge: bool,
    pub lights: Vec<String>,
    pub sensors: Vec<String>,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct PortalState {
    pub signedon: bool,
    pub incoming: bool,
    pub outgoing: bool,
    pub communication: String,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Backup {
    pub status: String,
    pub errorcode: i64,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct User {
    pub name: String,
    #[serde(rename = "last use date")]
    pub last_use_date: String,
    #[serde(rename = "create date")]
    pub create_date: String,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct Scene {
    pub name: String,
    pub lights: Vec<String>,
    pub owner: String,
    pub recycle: bool,
    pub locked: bool,
    pub appdata: AppData,
    pub picture: String,
    pub lastupdated: String,
    pub version: i64,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub struct AppData {
    pub version: i64,
    pub data: String,
}
