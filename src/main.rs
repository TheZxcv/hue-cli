extern crate clap;
extern crate dirs;
extern crate reqwest;
extern crate serde;
extern crate serde_json;

use clap::{App, Arg, SubCommand};

#[macro_use]
mod enum_util;
mod client_hue;
mod datatype;

use self::datatype::HueCliSettings;

fn wait_for_enter() {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).ok();
}

fn settings_file() -> std::path::PathBuf {
    let mut path = dirs::config_dir().unwrap();
    path.push("hue-cli");
    std::fs::create_dir_all(&path).unwrap();
    path.push("settings.json");
    path
}

fn load_settings() -> Option<HueCliSettings> {
    std::fs::File::open(settings_file())
        .ok()
        .and_then(|file| serde_json::from_reader(file).ok())
}

fn save_settings(settings: &HueCliSettings) -> Result<(), std::io::Error> {
    let file = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .open(settings_file())?;
    serde_json::to_writer(file, &settings)
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))
}

fn main() -> Result<(), client_hue::HueError> {
    let mut dirty = false;
    let mut settings: HueCliSettings = load_settings().unwrap_or(HueCliSettings::default());
    let ip: std::net::IpAddr = match &settings.bridge_ip {
        Some(ip) => ip.parse().unwrap(),
        None => {
            let ip = client_hue::ClientHue::discovery()?;
            settings.bridge_ip = Some(format!("{}", ip));
            dirty = true;
            ip
        }
    };

    let token = match &settings.token {
        None => {
            println!("Please press the link button on your bridge then press Enter...");
            wait_for_enter();
            let token = client_hue::ClientHue::auth(ip)?;
            settings.token = Some(token.to_owned());
            dirty = true;
            token
        }
        Some(tok) => tok.to_string(),
    };

    if dirty {
        save_settings(&settings).unwrap();
    }

    let matches = App::new("Hue-cli")
        .subcommand(
            SubCommand::with_name("light")
                .arg(Arg::with_name("light_id").index(1).required(false))
                .subcommand(SubCommand::with_name("on"))
                .subcommand(SubCommand::with_name("off")),
        )
        .subcommand(SubCommand::with_name("config"))
        .get_matches();

    let client = client_hue::ClientHue::new(ip, &token)?;

    match matches.subcommand() {
        ("light", Some(light_matches)) => {
            if let Some(light_id) = light_matches.value_of("light_id") {
                let result = match light_matches.subcommand() {
                    ("on", _) => Some(datatype::LightState {
                        on: Some(true),
                        ..Default::default()
                    }),
                    ("off", _) => Some(datatype::LightState {
                        on: Some(false),
                        ..Default::default()
                    }),
                    ("", None) => None,
                    _ => unreachable!(),
                };

                if let Some(state) = result {
                    client.update_light(light_id, state)?;
                } else {
                    let light = client.light(light_id)?;
                    println!("{}: {}", light_id, light);
                }
            } else {
                let lights = client.lights()?;
                lights.iter().for_each(|(id, light)| {
                    println!("{}: {}", id, light);
                });
            }
        }
        ("config", _config_matches) => {
            let config = client.config()?;
            println!("{:?}", config);
        }
        ("", None) => println!("No subcommand was used"),
        _ => unreachable!(),
    };

    Ok(())
}
