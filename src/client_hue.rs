extern crate reqwest;
extern crate serde;
extern crate ssdp;

use ssdp::header::{HeaderMut, Man, MX, ST};
use ssdp::message::{Multicast, SearchRequest};
use ssdp::FieldMap;
use std::net::IpAddr;

use crate::datatype::*;
use std::collections::HashMap;

type HueResult<T> = std::result::Result<T, HueError>;

#[derive(Debug, Clone)]
pub struct HueError {
    reason: String,
}

impl HueError {
    pub fn new(reason: &str) -> HueError {
        HueError {
            reason: reason.to_owned(),
        }
    }
}

impl std::fmt::Display for HueError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "HueError: {}", self.reason)
    }
}

impl std::error::Error for HueError {
    fn description(&self) -> &str {
        &self.reason
    }
}

pub struct ClientHue {
    client: reqwest::Client,
    #[allow(unused)]
    token: String,
    base_url: String,
}

impl ClientHue {
    pub fn new(address: IpAddr, token: &str) -> HueResult<ClientHue> {
        let web_client = reqwest::ClientBuilder::new()
            .http1_title_case_headers()
            .build()
            .map_err(|_| HueError::new("Could not create web client"))?;

        let addr = format!("{}", address);
        Ok(ClientHue {
            client: web_client,
            token: String::from(token),
            base_url: String::from(format!(
                "http://{address}/api/{token}",
                address = addr,
                token = token
            )),
        })
    }

    pub fn auth(address: IpAddr) -> HueResult<String> {
        let web_client = reqwest::ClientBuilder::new()
            .build()
            .map_err(|_| HueError::new("Could not create web client"))?;
        let base_url = format!("http://{}/api", address);
        let res: Response<CreatedUser> = web_client
            .post(&base_url)
            .json(&CreateUser {
                devicetype: "hue-cli#pc".to_owned(),
            })
            .send()
            .map_err(|_| HueError::new("Could not send request"))?
            .json()
            .map_err(|_| HueError::new("Could not authenticate"))?;

        match &res[0] {
            ResponseStatus::Success(resp) => Ok(resp.username.to_owned()),
            ResponseStatus::Error(resp) => Err(HueError::new(&resp.description)),
        }
    }

    pub fn discovery() -> HueResult<std::net::IpAddr> {
        let mut request = SearchRequest::new();
        request.set(Man);
        request.set(MX(5));
        request.set(ST::Target(FieldMap::upnp("IpBridge")));

        let ips = request
            .multicast()
            .map_err(|_| HueError::new("Failed to do things"))?
            .into_iter()
            .map(|(_, src)| src.ip())
            .collect::<Vec<_>>();

        if ips.len() > 0 {
            Ok(ips[0])
        } else {
            Err(HueError::new("Could not find a Hue bridge"))
        }
    }

    pub fn config(&self) -> HueResult<BridgeConfig> {
        self.client
            .get(&self.base_url)
            .send()
            .map_err(|_| HueError::new("Could not send request"))?
            .json()
            .map_err(|e| HueError::new(&format!("Could not get Bridge config: {}", e)))
    }

    pub fn lights(&self) -> HueResult<HashMap<String, Light>> {
        let url = format!("{base_url}/lights", base_url = self.base_url);
        self.client
            .get(&url)
            .send()
            .map_err(|_| HueError::new("Could not send request"))?
            .json()
            .map_err(|e| HueError::new(&format!("Could not get lights state: {}", e)))
    }

    pub fn light(&self, light_id: &str) -> HueResult<Light> {
        let url = format!(
            "{base_url}/lights/{light_id}",
            base_url = self.base_url,
            light_id = light_id
        );
        self.client
            .get(&url)
            .send()
            .map_err(|_| HueError::new("Could not send request"))?
            .json()
            .map_err(|e| HueError::new(&format!("Could not get light state: {}", e)))
    }

    pub fn update_light(&self, light_id: &str, state: LightState) -> HueResult<()> {
        let url = format!(
            "{base_url}/lights/{light_id}/state",
            base_url = self.base_url,
            light_id = light_id
        );

        let res: Response<ResourceUpdated> = self
            .client
            .put(&url)
            .json(&state)
            .send()
            .map_err(|_| HueError::new("Could not send request"))?
            .json()
            .map_err(|e| HueError::new(&format!("Could not update light state: {}", e)))?;

        match &res[0] {
            ResponseStatus::Success(_) => Ok(()),
            ResponseStatus::Error(err) => Err(HueError::new(&err.description)),
        }
    }
}
